### วิธีการติดตั้งตัว git ftp global command สำหรับวินโดว์

1. ให้พิมพ์คำสั่งตามนี้ลงใน <code>git bash</code> ทีละบรรทัด

```bash
    cd $HOME/Desktop && mkdir git-ftp-global

    curl https://raw.githubusercontent.com/git-ftp/git-ftp/master/git-ftp > $HOME/Desktop/git-ftp-global/git-ftp

    chmod +x $HOME/Desktop/git-ftp-global/git-ftp
```

2. จากนั้นให้ copy โฟร์เดอร์ git-ftp-global ที่อยู่หน้า Desktop ไปไว้ใน drive ที่ต้องการเช่น c:/git-ftp-global หรือ d:/git-ftp-global

3. ต่อจากนั้นให้ไป browse folder เข้าไปยัง system environment path ของวินโดว์เพื่อให้สามารถรันคำสั่ง bash ของ git ftp ได้ โดยทำตามตัวอย่างได้จากลิงค์ด้านล่างนี้

https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/

<hr>

### วิธีติดตั้งตัว git ftp global command สำหรับ Mac OS

https://brewinstall.org/install-git-ftp-on-mac-with-brew/

<hr>

### วิธีการใช้งาน git ftp on server ราย project

1. ให้ clone ตัว script bash ที่จะต้องใช้งานโดยอาจจะเอาไว้ที่ Desktop ก็ได้ โดยใช้คำสั่งนี้ ใน git bash

```bash
    cd $HOME/Desktop/ && git clone https://gitlab.com/netclub17/git-ftp-bash-script.git

    chmod +x $HOME/Desktop/git-ftp-bash-script/bin/*.sh
```

2. จากนั้นให้ copy 2 โฟร์เดอร์หลักที่ต้องใช้นำไปใส่ใน project ของเรา คือโฟร์เดอร์ <code>/bin</code> และ <code>/gftp</code>

3. เมื่อ copy ไปใส่เรียบร้อยแล้วจากนั้นให้เข้าไปในโฟร์เดอร์ <code>/gftp</code> แล้วเปลี่ยนชื่อไฟล์จาก <code>ftp-setting.example.conf</code> เป็น <code>ftp-setting.conf</code> แล้วเปิดไฟล์นี้ขึ้นมาแก้ไข <code>FTP_URL, FTP_USER, FTP_PASSWORD</code>

4. หลังจากที่เราได้แก้ config ของไฟล์ ftp-setting.conf เสร็จแล้วให้เราเปิด <code>git bash</code> ตอนอยู่ในโฟร์เดอร์ project ของเราขึ้นมาแล้วพิมพ์คำสั่งดังนี้

```bash
    chmod +x ./bin/fp-setting.sh

    ./bin/fp-setting.sh
```

5. เมื่อทำการ config setting เสร็จก็จะขึ้นประมาณว่า Success บลา บลา

6. จากนั้นก็ใช้งานสเต็ป git ตามปกติ แต่เปลี่ยนวิธีการจากตอนใช้ <code>git push origin master</code> เป็น <code>git fps</code> แทน

ตัวอย่างเช่น

```bash
    git add .

    git commit -m 'last fix html'

    git fps
```


ถ้าทุกอย่างปกติดีก็จะขึ้นสถานะของ git และมีข้อความประมาณว่า uploading... deploy from (previous-commit-sha) to (latest-commit-sha)


<code>* หมายเหตุ : สคริป ./bin/fp-setting.sh ไม่จำเป็นต้องรันทุกครั้ง ยกเว้นทุกครั้งที่การแก้ไข ภายในไฟล์ ftp-setting.conf ใหม่</code>
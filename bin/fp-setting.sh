#!/bin/bash
# Bash script setting default variable git-ftp BY NETCLUBYO

# get folder project name
DIRECTORY_NAME=${PWD##*/}

if [ -f "gftp/ftp-setting.example.conf" ]
then
    echo "Error : You can rename file config name from 'ftp-setting.example.conf' to 'ftp-setting.conf' only !!!"
    exit
fi

if [ -d "gftp/" ]
then
    cp -r gftp/ $HOME/
fi

if [ -f "${HOME}/gftp/ftp-setting.conf" ]
then
    mv ${HOME}/gftp/ftp-setting.conf ${HOME}/gftp/ftp-setting-${DIRECTORY_NAME}.conf
fi

# if check file config of folder name project
PATH_FTP_SETTING=""
if [ -f "${HOME}/gftp/ftp-setting-${DIRECTORY_NAME}.conf" ]
then
    PATH_FTP_SETTING="${HOME}/gftp/ftp-setting-${DIRECTORY_NAME}.conf"
fi

if [ -f "gftp/ftp-setting.conf" ]
then    
    PATH_FTP_SETTING="./gftp/ftp-setting.conf"
fi

if [ -z "$PATH_FTP_SETTING" ]
then
    echo "Error : ftp-setting.conf file not found !!!"
    exit
fi

source $PATH_FTP_SETTING

# Clear section old config
git config --remove-section git-ftp

git config git-ftp.url "${FTP_URL}" /
git config git-ftp.user "${FTP_USER}" /
git config git-ftp.password "${FTP_PASSWORD}" /
git config git-ftp.syncroot . /

if [ "$FTP_URL" ]
then
    # set alias commands push on repository && push on server
    git config alias.fps '!git push origin master && git ftp push'

    echo "FTP URL is $(git config git-ftp.url)"
    echo "FTP User is $(git config git-ftp.user)"
    echo "FTP Password is $(git config git-ftp.password)"
    echo ""
    echo "Success Configuration !!!"

    if [ -d "gftp/" ]
    then
        rm -rf gftp/
    fi
    
    # create log file check if exists on server and do not upload any file
    echo ""
    echo "Checking status .git-ftp.log file on server..."
    git ftp catchup
else
    echo "Fail something config !!!"
fi
#!/bin/bash

# set sycn root project on my local
git config git-ftp.syncroot . /

# create log file on server
git ftp catchup

# push on repository && push to server
git push origin master && git ftp push